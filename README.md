# Higher or lower

This is the first application I ever created. Originally it was written in JavaScript and I decided to reimplement it in C++.

The goal is to get to 500 points. The CPU rolls the dice, you decide whether you think you'll throw higher or lower than the CPU. If you're right you win points, if you're wrong you lose points.

![](.gifs/higher-lower.gif)

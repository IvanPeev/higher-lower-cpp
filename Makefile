# Source: https://stackoverflow.com/questions/30573481/path-include-and-src-directory-makefile

CC		 	 := g++
CPPFLAGS := -Iinclude -MMD -MP
CFLAGS 	 := -Wall
LDFLAGS	 := -Llib
CXXLIBS	 := -lm -lsfml-graphics -lsfml-window -lsfml-system

SRC_DIR := src
OBJ_DIR := obj
BIN_DIR := bin

EXE := $(BIN_DIR)/higher-lower
SRC := $(wildcard $(SRC_DIR)/*.cpp)
OBJ := $(SRC:$(SRC_DIR)/%.cpp=$(OBJ_DIR)/%.o)

.PHONY: all clean

all: $(EXE)

$(EXE): $(OBJ) | $(BIN_DIR)
	$(CC) $^ -o $@ $(LDFLAGS) $(CXXLIBS)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp $(OBJ_DIR)
	$(CC) $(DEP) $(CPPFLAGS) $(CXXLIBS) -c $< -o $@

$(BIN_DIR) $(OBJ_DIR):
	mkdir -p $@

clean:
	@$(RM) -rv $(BIN_DIR) $(OBJ_DIR)

-include $(OBJ:.o=.d)

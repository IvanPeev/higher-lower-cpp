#ifndef GAME_H_
#define GAME_H_

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <iostream>
#include <string>

#include "die.h"
#include "graphics-static.h"
#include "color.h"
#include "settings.h"

enum GameStep {
	STEP_NEW = 0,
	STEP_CPU = 1,
	STEP_BET = 2,
	STEP_ROUND_END = 3,
	STEP_GAME_END = 4
};

enum GameBet {
	BET_NONE = 0,
	BET_HIGH = 1,
	BET_LOW = 2
};

class Game {
	public:
		sf::Text message;
		sf::Text potText;
		sf::Text stakeText;
		sf::Text betText;
		sf::Text colorText;
		sf::RectangleShape background;
		sf::Font font;
		Color color;

		GameStep currentStep = STEP_NEW;
		GameBet bet = BET_NONE;
		int pot = STARTING_POT;
		int bid = 0;
		Die cpuDie1 = Die(CPU_POS_1);
		Die cpuDie2 = Die(CPU_POS_2);
		Die playerDie1 = Die(PLAYER_POS_1);
		Die playerDie2 = Die(PLAYER_POS_2);

		Game();

		void startWindow();
		void handleWindowDrawing();
		void drawDynamicGraphics();
		void drawStaticGraphics();
		void handleKeyEvent(char);

		void rollCPUDice();
		void rollPlayerDice();
		void checkPot();

		void setMessage();
		void setPotText();
		void setStakeText();
		void setBetText();
		void setColorText();

		void setBetHigh();
		void setBetLow();
		void setStepBet();
		void increaseBid();
		void decreaseBid();
		void createNewSet();
		void reset();

		void toggleForegroundColor();
};

#endif

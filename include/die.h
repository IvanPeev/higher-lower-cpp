#ifndef DIE_H_
#define DIE_H_

#include <SFML/Graphics.hpp>
#include <stdlib.h>
#include <vector>

#include "color.h"
#include "settings.h"

enum DiePosition {
	CPU_POS_1 = 35,
	CPU_POS_2 = 155,
	PLAYER_POS_1 = 435,
	PLAYER_POS_2 = 555
};

class Die {
	public:
		DiePosition position;
		Color color;
		int value = 0;
		sf::RectangleShape square;
		std::vector<sf::CircleShape> eyes;

		Die(DiePosition);

		void roll();
		void reset();
		void handleColorChange(Color);
	private:
		int getRandomDiceInt();
		void setSquare();
		void setFace(int, int, int);
		sf::CircleShape setEye(int, int, int, int);
};

#endif

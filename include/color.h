#ifndef COLOR_H_
#define COLOR_H_
#define COLORS_LENGTH 7

#include <SFML/Graphics.hpp>
#include <string>

class Color {
	public:
		void nextForegroundColor();
		sf::Color getBackgroundColor();
		sf::Color getForegroundColor();
		std::string getColorText();
	private:
		int FG_COLOR_IDX = 0;

		/**
		 * NOTE:
		 * SFML available predefined colors
		 * Black, White, Red, Green, Blue, Yellow, Magenta, Cyan
		 */
		sf::Color colors[COLORS_LENGTH] = {
			sf::Color::White,
			sf::Color::Red,
			sf::Color::Green,
			sf::Color::Blue,
			sf::Color::Yellow,
			sf::Color::Magenta,
			sf::Color::Cyan
		};

		std::string colorText[COLORS_LENGTH] = {
			"White",
			"Red",
			"Green",
			"Blue",
			"Yellow",
			"Magenta",
			"Cyan"
		};
};

#endif

#ifndef GRAPHICS_STATIC_H_
#define GRAPHICS_STATIC_H_

#include <SFML/Graphics.hpp>
#include "color.h"
#include "settings.h"

class GraphicsStatic {
	Color color;
	sf::Font font;
	public:
		GraphicsStatic(sf::Font, Color);
		sf::RectangleShape createFieldOutline();
		sf::RectangleShape createScoreOutline();
		sf::RectangleShape createScoreVertLine();
		sf::RectangleShape createScoreHorzLine();
		sf::RectangleShape createBetOutline();
		sf::RectangleShape createMessageOutline();
		sf::Text createTitle();
		sf::Text createQuitText();
		sf::Text createColorsText();
		sf::Text createPotTitle();
		sf::Text createStakeTitle();
		sf::Text createBetTitle();
		sf::Text createCPUTitle();
		sf::Text createPlayerTitle();
};

#endif

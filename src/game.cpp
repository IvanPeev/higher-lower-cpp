#include "game.h"

// TODO: this file needs to be refactored into something cleaner

sf::RenderWindow window(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), "Higher or lower");

Game::Game() {
	if (!font.loadFromFile(FONT)) {
		exit(EXIT_FAILURE);
	}
	background = sf::RectangleShape(sf::Vector2f(WINDOW_WIDTH, WINDOW_HEIGHT));

	srand(time(NULL));
	startWindow();
}

void Game::startWindow() {
	while (window.isOpen()) {
		background.setFillColor(color.getBackgroundColor());
		sf::Event event;

		while (window.pollEvent(event)) {
			window.clear();

			if (event.type == sf::Event::Closed) {
				window.close();
			}

			if (event.type == sf::Event::TextEntered) {
				char key = static_cast<char>(event.text.unicode);
				handleKeyEvent(key);
			}

			handleWindowDrawing();
			window.display();
		}
	}
}

void Game::handleWindowDrawing() {
	drawStaticGraphics();
	drawDynamicGraphics();
	setMessage();
	setPotText();
	setStakeText();
	setBetText();

	Die dice[] = {cpuDie1, cpuDie2, playerDie1, playerDie2};

	for (Die die : dice) {
		die.handleColorChange(color);
		if (die.value > 0) {
			window.draw(die.square);
			for (auto circle: die.eyes) {
				window.draw(circle);
			}
		}
	}
}

void Game::drawStaticGraphics() {
	GraphicsStatic graphics = GraphicsStatic(font, color);
	window.draw(background);

	window.draw(graphics.createFieldOutline());
	window.draw(graphics.createScoreOutline());
	window.draw(graphics.createScoreVertLine());
	window.draw(graphics.createScoreHorzLine());
	window.draw(graphics.createBetOutline());
	window.draw(graphics.createMessageOutline());
	window.draw(graphics.createTitle());
	window.draw(graphics.createQuitText());
	window.draw(graphics.createColorsText());
	window.draw(graphics.createPotTitle());
	window.draw(graphics.createStakeTitle());
	window.draw(graphics.createBetTitle());
	window.draw(graphics.createCPUTitle());
	window.draw(graphics.createPlayerTitle());
}

void Game::drawDynamicGraphics() {
	message.setFont(font);
	message.setCharacterSize(16);
	message.setFillColor(color.getForegroundColor());
	message.setPosition(WINDOW_WIDTH - 480, WINDOW_HEIGHT - 110);

	potText.setFont(font);
	potText.setCharacterSize(16);
	potText.setFillColor(color.getForegroundColor());
	potText.setPosition(SCORE_OUTLINE_X + SCORE_OUTLINE_PADDING, SCORE_OUTLINE_Y + SCORE_OUTLINE_PADDING + 50);

	stakeText.setFont(font);
	stakeText.setCharacterSize(16);
	stakeText.setFillColor(color.getForegroundColor());
	stakeText.setPosition((SCORE_OUTLINE_X * 4) + SCORE_OUTLINE_PADDING - 5, SCORE_OUTLINE_Y + SCORE_OUTLINE_PADDING + 50);

	betText.setFont(font);
	betText.setCharacterSize(16);
	betText.setFillColor(color.getForegroundColor());
	betText.setPosition(WINDOW_WIDTH / 5 + 50, WINDOW_HEIGHT - 65);

	colorText.setFont(font);
	colorText.setCharacterSize(16);
	colorText.setFillColor(color.getForegroundColor());
	colorText.setPosition(10, 7);
	colorText.setString(color.getColorText());

	window.draw(message);
	window.draw(potText);
	window.draw(stakeText);
	window.draw(betText);
	window.draw(colorText);
}

void Game::setMessage() {
	if (currentStep == STEP_NEW) {
		message.setString("Press 'c' to roll for CPU");
	}

	if (currentStep == STEP_CPU || currentStep == STEP_BET) {
		message.setString("Use 'h' and 'l' to set your bet\nUse '<' and '>' to set your bid\nPress 'p' to roll for PLAYER");
	}

	if (currentStep == STEP_ROUND_END) {
		int cpuTotal = cpuDie1.value + cpuDie2.value;
		int playerTotal = playerDie1.value + playerDie2.value;

		if (cpuTotal == playerTotal) {
			message.setString("Round stalemate\nPress 'n' for a new round");
		} else {
			if (bet == BET_HIGH) {
				if (cpuTotal < playerTotal) {
					message.setString("Round won\nPress 'n' for a new round");
				} else if (cpuTotal > playerTotal) {
					message.setString("Round lost\nPress 'n' for a new round");
				}
			} else if (bet == BET_LOW) {
				if (cpuTotal < playerTotal) {
					message.setString("Round lost\nPress 'n' for a new round");
				} else if (cpuTotal > playerTotal) {
					message.setString("Round won\nPress 'n' for a new round");
				}
			}
		}
	}

	if (currentStep == STEP_GAME_END) {
		if (pot >= WINNING_POT) {
			message.setString("Game won\nPress 'r' for a new game");
		} else if (pot <= 0) {
			message.setString("Game lost\nPress 'r' for a new game");
		}
	}
}

void Game::setPotText() {
	potText.setString(std::to_string(pot));
}

void Game::setStakeText() {
	stakeText.setString(std::to_string(bid));
}

void Game::setBetText() {
	switch (bet) {
		case BET_NONE:
			betText.setString("None");
			break;
		case BET_HIGH:
			betText.setString("High");
			break;
		case BET_LOW:
			betText.setString("Low");
			break;
	}
}

void Game::handleKeyEvent(char key) {
	switch (key) {
		case 'q':
			window.close();
			break;
		case 'n':
			if (currentStep == STEP_ROUND_END) createNewSet();
			break;
		case 'r':
			reset();
			break;
		case 'c':
			if (currentStep == STEP_NEW) rollCPUDice();
			break;
		case 'p':
			if (currentStep == STEP_BET) rollPlayerDice();
			break;
		case 'h':
			if (currentStep == STEP_CPU || currentStep == STEP_BET) setBetHigh();
			break;
		case 'l':
			if (currentStep == STEP_CPU || currentStep == STEP_BET) setBetLow();
			break;
		case '.':
		case '>':
			if (currentStep == STEP_CPU || currentStep == STEP_BET) increaseBid();
			break;
		case ',':
		case '<':
			if (currentStep == STEP_CPU || currentStep == STEP_BET) decreaseBid();
			break;
		case 'f':
			toggleForegroundColor();
			break;
	}
}

void Game::rollCPUDice() {
	cpuDie1.roll();
	cpuDie2.roll();
	currentStep = STEP_CPU;
	setMessage();
}

void Game::rollPlayerDice() {
	playerDie1.roll();
	playerDie2.roll();
	checkPot();
	setMessage();
}

void Game::checkPot() {
	int cpuTotal = cpuDie1.value + cpuDie2.value;
	int playerTotal = playerDie1.value + playerDie2.value;

	if (cpuTotal == playerTotal) {
		currentStep = STEP_ROUND_END;
	} else {
		if (bet == BET_HIGH) {
			if (cpuTotal < playerTotal) {
				pot += bid;
			} else if (cpuTotal > playerTotal) {
				pot -= bid;
			}
		} else if (bet == BET_LOW) {
			if (cpuTotal < playerTotal) {
				pot -= bid;
			} else if (cpuTotal > playerTotal) {
				pot += bid;
			}
		}

		if (pot >= WINNING_POT || pot <= 0) {
			currentStep = STEP_GAME_END;
		} else {
			currentStep = STEP_ROUND_END;
		}
	}	
}

void Game::setBetHigh() {
	bet = BET_HIGH;
	setStepBet();
}

void Game::setBetLow() {
	bet = BET_LOW;
	setStepBet();
}

void Game::setStepBet() {
	currentStep = STEP_BET;
	setMessage();
}

void Game::increaseBid() {
	if (bid < pot) bid += BID_SIZE;
}

void Game::decreaseBid() {
	if ((bid - BID_SIZE) >= 0) bid -= BID_SIZE;
}

void Game::createNewSet() {
	bet = BET_NONE;
	bid = 0;
	currentStep = STEP_NEW;
	cpuDie1.reset();
	cpuDie2.reset();
	playerDie1.reset();
	playerDie2.reset();
	setMessage();
}

void Game::reset() {
	pot = STARTING_POT;
	createNewSet();
}

void Game::toggleForegroundColor() {
	color.nextForegroundColor();
}

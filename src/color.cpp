#include "color.h"
#include <iostream>

void Color::nextForegroundColor() {
	FG_COLOR_IDX = (FG_COLOR_IDX + 1) % COLORS_LENGTH;
}

sf::Color Color::getBackgroundColor() {
	return sf::Color::Black;
}

sf::Color Color::getForegroundColor() {
	return colors[FG_COLOR_IDX];
}

std::string Color::getColorText() {
	return colorText[FG_COLOR_IDX];
}

#include "graphics-static.h"

GraphicsStatic::GraphicsStatic(sf::Font fnt, Color col) : font(fnt), color(col) {}

sf::RectangleShape GraphicsStatic::createFieldOutline() {
	auto fieldOutline = sf::RectangleShape(sf::Vector2f(DICE_OUTLINE_WIDTH, DICE_OUTLINE_HEIGHT));
	fieldOutline.setFillColor(color.getBackgroundColor());
	fieldOutline.setOutlineColor(color.getForegroundColor());
	fieldOutline.setOutlineThickness(5.f);
	fieldOutline.setPosition(DICE_OUTLINE_X, DICE_OUTLINE_Y);
	return fieldOutline;
}

sf::RectangleShape GraphicsStatic::createScoreOutline() {
	auto scoreOutline = sf::RectangleShape(sf::Vector2f(SCORE_OUTLINE_WIDTH, SCORE_OUTLINE_HEIGHT));
	scoreOutline.setFillColor(color.getBackgroundColor());
	scoreOutline.setOutlineColor(color.getForegroundColor());
	scoreOutline.setOutlineThickness(3.f);
	scoreOutline.setPosition(SCORE_OUTLINE_X, SCORE_OUTLINE_Y);
	return scoreOutline;
}

sf::RectangleShape GraphicsStatic::createScoreVertLine() {
	sf::RectangleShape scoreVertLine(sf::Vector2f(3.f, 100.f));
	scoreVertLine.setFillColor(color.getForegroundColor());
	scoreVertLine.setPosition(SCORE_OUTLINE_X + (SCORE_OUTLINE_WIDTH / 2), SCORE_OUTLINE_Y);
	return scoreVertLine;
}

sf::RectangleShape GraphicsStatic::createScoreHorzLine() {
	sf::RectangleShape scoreHorzLine(sf::Vector2f(150.f, 3.f));
	scoreHorzLine.setFillColor(color.getForegroundColor());
	scoreHorzLine.setPosition(SCORE_OUTLINE_X, SCORE_OUTLINE_Y + SCORE_OUTLINE_PADDING * 3);
	return scoreHorzLine;
}

sf::RectangleShape GraphicsStatic::createBetOutline() {
	sf::RectangleShape betOutline(sf::Vector2f(200.f, 50.f));
	betOutline.setFillColor(color.getBackgroundColor());
	betOutline.setOutlineColor(color.getForegroundColor());
	betOutline.setOutlineThickness(3.f);
	betOutline.setPosition(WINDOW_WIDTH / 4 - 50, WINDOW_HEIGHT - 80);
	return betOutline;
}

sf::RectangleShape GraphicsStatic::createMessageOutline() {
	sf::RectangleShape betOutline(sf::Vector2f(470.f, 100.f));
	betOutline.setFillColor(color.getBackgroundColor());
	betOutline.setOutlineColor(color.getForegroundColor());
	betOutline.setOutlineThickness(3.f);
	betOutline.setPosition(WINDOW_WIDTH - 500, WINDOW_HEIGHT - 128);
	return betOutline;
}

sf::Text GraphicsStatic::createTitle() {
	sf::Text title;
	title.setFont(font);
	title.setString("Higher or lower");
	title.setCharacterSize(32);
	title.setFillColor(color.getForegroundColor());
	title.setPosition(WINDOW_WIDTH / 2 - 125, 100);
	return title;
}

sf::Text GraphicsStatic::createQuitText() {
	sf::Text quitText;
	quitText.setFont(font);
	quitText.setString("Press 'q' to exit");
	quitText.setCharacterSize(16);
	quitText.setFillColor(color.getForegroundColor());
	quitText.setPosition(DICE_OUTLINE_X, DICE_OUTLINE_Y - 30);
	return quitText;
}

sf::Text GraphicsStatic::createColorsText() {
	sf::Text colorsText;
	colorsText.setFont(font);
	colorsText.setString("Toggle between colors with 'f'");
	colorsText.setCharacterSize(16);
	colorsText.setFillColor(color.getForegroundColor());
	colorsText.setPosition(DICE_OUTLINE_WIDTH - DICE_OUTLINE_X + 15, DICE_OUTLINE_Y - 30);
	return colorsText;
}

sf::Text GraphicsStatic::createPotTitle() {
	sf::Text potTitle;
	potTitle.setFont(font);
	potTitle.setString("POT");
	potTitle.setCharacterSize(16);
	potTitle.setFillColor(color.getForegroundColor());
	potTitle.setPosition(SCORE_OUTLINE_X + SCORE_OUTLINE_PADDING * 1.25, SCORE_OUTLINE_Y + SCORE_OUTLINE_PADDING);
	return potTitle;
}

sf::Text GraphicsStatic::createStakeTitle() {
	sf::Text stakeTitle;
	stakeTitle.setFont(font);
	stakeTitle.setString("STAKE");
	stakeTitle.setCharacterSize(16);
	stakeTitle.setFillColor(color.getForegroundColor());
	stakeTitle.setPosition(SCORE_OUTLINE_WIDTH - SCORE_OUTLINE_PADDING * 2, SCORE_OUTLINE_Y + SCORE_OUTLINE_PADDING);
	return stakeTitle;
}

sf::Text GraphicsStatic::createBetTitle() {
	sf::Text betTitle;
	betTitle.setFont(font);
	betTitle.setString("BET:");
	betTitle.setCharacterSize(16);
	betTitle.setFillColor(color.getForegroundColor());
	betTitle.setPosition(WINDOW_WIDTH / 5 + 10, WINDOW_HEIGHT - 65);
	return betTitle;
}

sf::Text GraphicsStatic::createCPUTitle() {
	sf::Text cpuTitle;
	cpuTitle.setFont(font);
	cpuTitle.setString("CPU");
	cpuTitle.setCharacterSize(24);
	cpuTitle.setFillColor(color.getForegroundColor());
	cpuTitle.setPosition(
			DICE_OUTLINE_X + (DICE_OUTLINE_WIDTH / 4) - 50,
			DICE_OUTLINE_Y + DICE_OUTLINE_HEIGHT + 25);
	return cpuTitle;
}

sf::Text GraphicsStatic::createPlayerTitle() {
	sf::Text playerTitle;
	playerTitle.setFont(font);
	playerTitle.setString("PLAYER");
	playerTitle.setCharacterSize(24);
	playerTitle.setFillColor(color.getForegroundColor());
	playerTitle.setPosition(
			DICE_OUTLINE_X + (DICE_OUTLINE_WIDTH / 2) + (DICE_OUTLINE_WIDTH / 4) - 50,
			DICE_OUTLINE_Y + DICE_OUTLINE_HEIGHT + 25);
	return playerTitle;
}

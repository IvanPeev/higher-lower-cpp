#include "die.h"
#include <iostream>

Die::Die(DiePosition pos) : position(pos) {}

void Die::roll() {
	value = getRandomDiceInt();
	setSquare();
	setFace(DICE_OUTLINE_X + position + 20, DICE_OUTLINE_Y + (DICE_OUTLINE_HEIGHT / 4), value);
}

int Die::getRandomDiceInt() {
	return rand() % 6 + 1;
}

void Die::setSquare() {
	sf::RectangleShape squareOutline(sf::Vector2f(BOX_WIDTH, BOX_HEIGHT));
	squareOutline.setFillColor(color.getBackgroundColor());
	squareOutline.setOutlineColor(color.getForegroundColor());
	squareOutline.setOutlineThickness(3.f);
	squareOutline.setPosition(DICE_OUTLINE_X + position + 20, DICE_OUTLINE_Y + (DICE_OUTLINE_HEIGHT / 4));
	square = squareOutline;
}

void Die::setFace(int x, int y, int numEyes) {
	switch (numEyes) {
		case 1:
			eyes.push_back(setEye(2, 2, x, y));
			break;
		case 2:
			eyes.push_back(setEye(1, 3, x, y));
			eyes.push_back(setEye(3, 1, x, y));
			break;
		case 3:
			eyes.push_back(setEye(1, 3, x, y));
			eyes.push_back(setEye(2, 2, x, y));
			eyes.push_back(setEye(3, 1, x, y));
			break;
		case 4:
			eyes.push_back(setEye(1, 1, x, y));
			eyes.push_back(setEye(1, 3, x, y));
			eyes.push_back(setEye(3, 1, x, y));
			eyes.push_back(setEye(3, 3, x, y));
			break;
		case 5:
			eyes.push_back(setEye(1, 1, x, y));
			eyes.push_back(setEye(1, 3, x, y));
			eyes.push_back(setEye(2, 2, x, y));
			eyes.push_back(setEye(3, 1, x, y));
			eyes.push_back(setEye(3, 3, x, y));
			break;
		case 6:
			eyes.push_back(setEye(1, 1, x, y));
			eyes.push_back(setEye(1, 2, x, y));
			eyes.push_back(setEye(1, 3, x, y));
			eyes.push_back(setEye(3, 1, x, y));
			eyes.push_back(setEye(3, 2, x, y));
			eyes.push_back(setEye(3, 3, x, y));
			break;
	}
}

sf::CircleShape Die::setEye(int posX, int posY, int x, int y) {
	sf::CircleShape circle(RADIUS);
	float ratio = (BOX_WIDTH - 30) / 3;

	circle.setFillColor(color.getForegroundColor());
	circle.setPosition(x + (posX * ratio), y + (posY * ratio));

	return circle;
}

void Die::reset() {
	value = 0;
	square.setSize(sf::Vector2f(0, 0));
	eyes.clear();
}

void Die::handleColorChange(Color color) {
	Die::color = color;

	if (value > 0) {
		setSquare();
		setFace(DICE_OUTLINE_X + position + 20, DICE_OUTLINE_Y + (DICE_OUTLINE_HEIGHT / 4), value);
	}
}
